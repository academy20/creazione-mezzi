package it.prismaprogetti.academy.creazionemezzi.decoder;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import it.prismaprogetti.academy.creazionemezzi.error.TargaException;
import it.prismaprogetti.academy.creazionemezzi.error.autovetturaexception.CilindrataException;
import it.prismaprogetti.academy.creazionemezzi.error.autovetturaexception.PotenzaException;
import it.prismaprogetti.academy.creazionemezzi.model.autovettura.Cilindrata;
import it.prismaprogetti.academy.creazionemezzi.model.autovettura.ModelloAutovettura;
import it.prismaprogetti.academy.creazionemezzi.model.autovettura.ModelloConCaratteristiche;
import it.prismaprogetti.academy.creazionemezzi.model.autovettura.Potenza;
import it.prismaprogetti.academy.creazionemezzi.model.autovettura.TargaAutovettura;

public class DecoderAutomobili extends DecoderVeicoli {

	public DecoderAutomobili(char marcatore) {
		super(marcatore);
		// TODO Auto-generated constructor stub
	}

	public List<TargaAutovettura> readTarghe(String path) throws IOException, TargaException {

		List<TargaAutovettura> targheAutovetture = new ArrayList<>();

		try (Reader reader = new FileReader(new File(path)); BufferedReader br = new BufferedReader(reader)) {

			String targa = null;
			while ((targa = br.readLine()) != null) {

				TargaAutovettura targaAutovettura = TargaAutovettura.creaTarga(targa);
				targheAutovetture.add(targaAutovettura);
			}

		}

		return targheAutovetture;

	}

	public List<ModelloConCaratteristiche> readModelliConCaratteristiche(String path)
			throws IOException, CilindrataException, PotenzaException {

		List<ModelloConCaratteristiche> modelliConCaratteristiche = new ArrayList<>();

		try (Reader reader = new FileReader(new File(path)); BufferedReader br = new BufferedReader(reader)) {

			/*
			 * recupero i record
			 */
			String record = null;
			while ((record = br.readLine()) != null) { // Ferrari F430, 4300, 350, 2

				String[] recordArray = record.split(String.valueOf(this.getMarcatore())); // [Ferrari F430 | 4300 | 350 | 2]
				if (recordArray.length != 4) {
					System.err.println("il record � invalido " + record);
					continue;
				}

				ModelloAutovettura modelloAutovettura = ModelloAutovettura.creaModello(recordArray[0].trim());
				Cilindrata cilindrata = Cilindrata.creazioneMotoreAutovettura(recordArray[1].trim());
				Potenza potenza = Potenza.creazionePotenzaAutovettura(recordArray[2].trim());
				Integer posti = Integer.parseInt(recordArray[3].trim());

				ModelloConCaratteristiche modelloConCaratteristiche = new ModelloConCaratteristiche(modelloAutovettura,
						cilindrata, potenza, posti);
				modelliConCaratteristiche.add(modelloConCaratteristiche);

			}

		}

		return modelliConCaratteristiche;

	}

}
