package it.prismaprogetti.academy.creazionemezzi.model;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import it.prismaprogetti.academy.creazionemezzi.decoder.DecoderVeicoli;
import it.prismaprogetti.academy.creazionemezzi.decoder.DecoderAutomobili;
import it.prismaprogetti.academy.creazionemezzi.decoder.DecoderImbarcazione;
import it.prismaprogetti.academy.creazionemezzi.decoder.DecoderVelivoli;
import it.prismaprogetti.academy.creazionemezzi.error.DataException;
import it.prismaprogetti.academy.creazionemezzi.error.TargaException;
import it.prismaprogetti.academy.creazionemezzi.error.autovetturaexception.CilindrataException;
import it.prismaprogetti.academy.creazionemezzi.error.autovetturaexception.PotenzaException;
import it.prismaprogetti.academy.creazionemezzi.error.imbarcazioneexception.CaricoMaxException;
import it.prismaprogetti.academy.creazionemezzi.error.imbarcazioneexception.VelocitaMaxException;
import it.prismaprogetti.academy.creazionemezzi.model.autovettura.Autovettura;
import it.prismaprogetti.academy.creazionemezzi.model.autovettura.ModelloConCaratteristiche;
import it.prismaprogetti.academy.creazionemezzi.model.autovettura.TargaAutovettura;
import it.prismaprogetti.academy.creazionemezzi.model.imbarcazione.Imbarcazione;
import it.prismaprogetti.academy.creazionemezzi.model.imbarcazione.ImbarcazioneConCaratteristiche;
import it.prismaprogetti.academy.creazionemezzi.model.imbarcazione.TargaImbarcazione;
import it.prismaprogetti.academy.creazionemezzi.model.velivolo.Velivolo;
import it.prismaprogetti.academy.creazionemezzi.repository.InMemoryRepository;
import it.prismaprogetti.academy.creazionemezzi.utility.RandomUtility;

/**
 * classe adibita all'inserimento dati nel repository oppure alla creazione di
 * MULTI-oggetti casuali e non
 * 
 * @author patri
 *
 */
public class VeicoliManager {

	private final String directoryScriviSuFile;
	/*
	 * decoder generico
	 */
	private final DecoderVeicoli decoderVeicoli;
	private final String pathDateImmatricolazioni;

	private VeicoliManager(DecoderVeicoli decoderVeicoli, String directoryScriviSuFile, String pathDateImmatricolazioni) {
		super();
		this.decoderVeicoli = decoderVeicoli;
		this.directoryScriviSuFile = directoryScriviSuFile;
		this.pathDateImmatricolazioni = pathDateImmatricolazioni;
	}

	public static VeicoliManager crea(DecoderVeicoli decoderVeicoli, String directoryScriviSuFile,
			String pathDateImmatricolazioni) throws IOException, DataException {

		decoderVeicoli.checkDateImmatricolazioniSeSuccessiveAllaDataDiOggi(pathDateImmatricolazioni);

		return new VeicoliManager(decoderVeicoli, directoryScriviSuFile, pathDateImmatricolazioni);

	}

	public static void inserisciVeicoloNelRepository(InMemoryRepository inMemoryRepository, Veicolo veicolo) {

		Veicolo veicoloByTarga = null;

		if (veicolo instanceof Autovettura) {
			Autovettura autovettura = (Autovettura) veicolo;

			veicoloByTarga = inMemoryRepository.getVeicoloByTarga(autovettura.getTarga().getCodice());
		}

		if (veicolo instanceof Velivolo) {
			Velivolo velivolo = (Velivolo) veicolo;
			veicoloByTarga = inMemoryRepository.getVeicoloByTarga(velivolo.getCodice().getCodice());

		}

		if (veicolo instanceof Imbarcazione) {
			Imbarcazione imbarcazione = (Imbarcazione) veicolo;
			veicoloByTarga = inMemoryRepository.getVeicoloByTarga(imbarcazione.getTarga().getCodice());
		}

		if (veicoloByTarga != null) {
			throw new IllegalStateException("impossibile salvare il veicolo nel Db perch� gi� presente");
		}

		inMemoryRepository.save(veicolo);

	}

	public List<Autovettura> creaAutomobiliDaFile(DecoderAutomobili decoderAutomobili,
			String pathModelliConCaratteristiche, String pathTargheAuto)
			throws IOException, CilindrataException, PotenzaException, TargaException, DataException {

		List<ModelloConCaratteristiche> modelliConCaratteristiche = decoderAutomobili
				.readModelliConCaratteristiche(pathModelliConCaratteristiche);

		List<DataImmatricolazione> dateImmatricolazione = decoderAutomobili
				.readDateImmatricolazioni(this.pathDateImmatricolazioni);

		List<TargaAutovettura> targhe = decoderAutomobili.readTarghe(pathTargheAuto);

		List<Autovettura> autovetture = new ArrayList<>(modelliConCaratteristiche.size());
		for (int i = 0; i < modelliConCaratteristiche.size(); i++) {

			int indiceCasualeDataImmatricolazione = RandomUtility.randomNumber(0, dateImmatricolazione.size() - 1);
			int indiceCasualeTarghe = RandomUtility.randomNumber(0, targhe.size() - 1);

			Autovettura autovettura = Autovettura.creaAutomobile(
					dateImmatricolazione.get(indiceCasualeDataImmatricolazione), targhe.get(indiceCasualeTarghe),
					modelliConCaratteristiche.get(i).getModelloAutovettura(),
					modelliConCaratteristiche.get(i).getCilindrata(), modelliConCaratteristiche.get(i).getKw(),
					modelliConCaratteristiche.get(i).getPosti());

			autovetture.add(autovettura);
		}

		return autovetture;

	}

	public List<Imbarcazione> creaImbarcazioniDaFile(DecoderImbarcazione decoderImbarcazione,
			String pathImbarcazioniConCaratteristiche, String pathTargheImbarcazioni)
			throws IOException, CaricoMaxException, VelocitaMaxException, TargaException, DataException {

		List<ImbarcazioneConCaratteristiche> imbarcazioniConCaratteristiche = decoderImbarcazione
				.readImbarcazioniConCaratteristiche(pathImbarcazioniConCaratteristiche);

		List<DataImmatricolazione> dateImmatricolazione = decoderImbarcazione
				.readDateImmatricolazioni(this.pathDateImmatricolazioni);

		List<TargaImbarcazione> targhe = decoderImbarcazione.readTarghe(pathTargheImbarcazioni);

		List<Imbarcazione> imbarcazioni = new ArrayList<>(imbarcazioniConCaratteristiche.size());
		for (int i = 0; i < imbarcazioniConCaratteristiche.size(); i++) {

			int indiceCasualeDataImmatricolazione = RandomUtility.randomNumber(0, dateImmatricolazione.size() - 1);
			int indiceCasualeTarghe = RandomUtility.randomNumber(0, targhe.size() - 1);

			Imbarcazione imbarcazione = Imbarcazione.creaImbarcazione(
					dateImmatricolazione.get(indiceCasualeDataImmatricolazione), targhe.get(indiceCasualeTarghe),
					imbarcazioniConCaratteristiche.get(i).getNome(),
					imbarcazioniConCaratteristiche.get(i).getVelocitaMax(),
					imbarcazioniConCaratteristiche.get(i).getCaricoMax());

			imbarcazioni.add(imbarcazione);
		}

		return imbarcazioni;

	}

	public List<Velivolo> creaVelivoliDaFile(DecoderVelivoli decoderVelivoli, String pathVelivoli)
			throws IOException, DataException {

		return decoderVelivoli.readVelivoliFromFile(pathVelivoli);
	}

	/**
	 * verifica se la data del file veicoli � piu grande della data immatricolazione
	 * del veicoli passato come parametro
	 * 
	 * @param file
	 * @param veicolo
	 */
	public static void checkDataFileVeicoliIsGreaterThanDataImmatricolazione(File file, Veicolo veicolo) {

		String nomeVeicoliFile = file.getName();

		Pattern pattern = Pattern.compile("\\d+");
		Matcher matcher = pattern.matcher(nomeVeicoliFile);

		LocalDate localDateFile = null;

		if (matcher.find()) {

			String dataDelFileVeicoli = matcher.group().trim();

			Integer anno = Integer.parseInt(dataDelFileVeicoli.substring(0, 4));
			Integer mese = Integer.parseInt(dataDelFileVeicoli.substring(4, 6));
			Integer giorno = Integer.parseInt(dataDelFileVeicoli.substring(6, 8));

			localDateFile = LocalDate.of(anno, mese, giorno);
		}

		LocalDate localDateVeicolo = veicolo.getDataImmatricolazione().getData();

		if (localDateVeicolo.isAfter(localDateFile)) {
			throw new IllegalStateException("la data immatricolazione del veicolo " + veicolo
					+ " non pu� essere superiore alla data del file veicoli");
		}

	}

	public void creaFileVeicoliDatEScriviVeicoli(List<Veicolo> veicoli) throws IOException {

		/*
		 * creazione veicoli formato dat
		 */
		File fileVeicoliDat = this.decoderVeicoli.creaFileDatVeicoli(this.directoryScriviSuFile);
		fileVeicoliDat.createNewFile();

		/*
		 * verifico che ogni veicoli che verr� inserito non abbia una data superiore a
		 * quella del file dat
		 */
		for (Veicolo veicolo : veicoli) {
			checkDataFileVeicoliIsGreaterThanDataImmatricolazione(fileVeicoliDat, veicolo);
		}

		/*
		 * inserisco veicoli nel file dat
		 */
		this.decoderVeicoli.scriviSuFileVeicoliDatByList(fileVeicoliDat, veicoli);
	}

}
