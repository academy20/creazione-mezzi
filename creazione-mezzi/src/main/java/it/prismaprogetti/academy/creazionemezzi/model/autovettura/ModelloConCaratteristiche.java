package it.prismaprogetti.academy.creazionemezzi.model.autovettura;

/**
 * DTO = data transfer object
 * @author patri
 *
 */
public class ModelloConCaratteristiche {

	private ModelloAutovettura modelloAutovettura;
	private Cilindrata cilindrata;
	private Potenza kw;
	private int posti;

	public ModelloConCaratteristiche(ModelloAutovettura modelloAutovettura, Cilindrata cilindrata, Potenza kw,
			int posti) {
		super();
		this.modelloAutovettura = modelloAutovettura;
		this.cilindrata = cilindrata;
		this.kw = kw;
		this.posti = posti;
	}

	public ModelloAutovettura getModelloAutovettura() {
		return modelloAutovettura;
	}

	public Cilindrata getCilindrata() {
		return cilindrata;
	}

	public Potenza getKw() {
		return kw;
	}

	public int getPosti() {
		return posti;
	}

}
