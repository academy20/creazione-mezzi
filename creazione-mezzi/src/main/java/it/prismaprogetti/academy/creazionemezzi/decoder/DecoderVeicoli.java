package it.prismaprogetti.academy.creazionemezzi.decoder;

import static it.prismaprogetti.academy.creazionemezzi.model.Tipo.A;
import static it.prismaprogetti.academy.creazionemezzi.model.Tipo.I;
import static it.prismaprogetti.academy.creazionemezzi.model.Tipo.V;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import it.prismaprogetti.academy.creazionemezzi.error.DataException;
import it.prismaprogetti.academy.creazionemezzi.model.DataImmatricolazione;
import it.prismaprogetti.academy.creazionemezzi.model.Veicolo;
import it.prismaprogetti.academy.creazionemezzi.model.autovettura.Autovettura;
import it.prismaprogetti.academy.creazionemezzi.model.imbarcazione.Imbarcazione;
import it.prismaprogetti.academy.creazionemezzi.model.velivolo.Velivolo;
import it.prismaprogetti.academy.creazionemezzi.repository.InMemoryRepository;

public class DecoderVeicoli {

	private final char marcatore;

	public DecoderVeicoli(char marcatore) {
		super();
		this.marcatore = marcatore;
	}

	public char getMarcatore() {
		return marcatore;
	}

	public void checkDateImmatricolazioniSeSuccessiveAllaDataDiOggi(String pathDateImmatricolazioni)
			throws IOException, DataException {

		LocalDate dataDiOggi = LocalDate.now();

		try (Reader reader = new FileReader(new File(pathDateImmatricolazioni));
				BufferedReader br = new BufferedReader(reader)) {

			String data = null;
			while ((data = br.readLine()) != null) {

				/*
				 * grazie a questo metodo sono sicuro che il formato della data deve essere per forza come il seguente : yyyyMMdd
				 */
				DataImmatricolazione.checkFormatData(data);
				
				/*
				 * converto string --> localDate
				 */
				LocalDate dataImmatricolazione = DataImmatricolazione.convertStringToData(data);

				/*
				 * verifico se la data immatricolazione � successiva alla data di oggi
				 */
				if (dataImmatricolazione.isAfter(dataDiOggi)) {

					throw new IllegalStateException(
							"la data di immatricolazione non pu� essere successiva alla data di oggi");
				}

			}

		}

	}

	public List<DataImmatricolazione> readDateImmatricolazioni(String pathDateImmatricolazioni)
			throws IOException, DataException {

		List<DataImmatricolazione> dateImmatricolazioni = new ArrayList<>();

		try (Reader reader = new FileReader(new File(pathDateImmatricolazioni));
				BufferedReader br = new BufferedReader(reader)) {

			String data = null;
			while ((data = br.readLine()) != null) {

				DataImmatricolazione.checkIsNotNull(data);
				DataImmatricolazione.checkFormatData(data);

				DataImmatricolazione dataImmatricolazione = DataImmatricolazione.creaData(data);

				dateImmatricolazioni.add(dataImmatricolazione);

			}

		}

		return dateImmatricolazioni;

	}

	public File creaFileDatVeicoli(String directoryDiDestinazione) {

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd_HH.mm.ss").withLocale(Locale.ITALY);
		String dataEOrarioDelFile = LocalDateTime.now().format(formatter);
		String nomeFile = "veicoli-" + dataEOrarioDelFile + ".dat";
		String path = directoryDiDestinazione + "\\" + nomeFile;

		return new File(path);
	}

	public void scriviSuFileVeicoliDatByRepository(File file, InMemoryRepository inMemoryRepository, int qtaVeicoli)
			throws IOException {

		List<Veicolo> veicoliCasuali = inMemoryRepository.getVeicoliCasuali(qtaVeicoli);

		try (Writer writer = new OutputStreamWriter(new FileOutputStream(file));
				BufferedWriter bw = new BufferedWriter(writer);) {

			for (Veicolo veicolo : veicoliCasuali) {

				/*
				 * StringBuffer e StringBuilder non lavora tramite reference StringBuffer � piu
				 * lento di StringBuilder ma � ThreadSafe
				 */
				StringBuffer datiVeicolo = new StringBuffer("");

				switch (veicolo.getTipo()) {
				case A:

					Autovettura auto = (Autovettura) veicolo;
					datiVeicolo.replace(0, datiVeicolo.length(),
							A + "|" + auto.getTarga().getCodice() + "|" + auto.getModello().getNome() + "|"
									+ auto.getCilindrata().getDimensioneMotore() + "|" + auto.getKw().getKw() + "|"
									+ auto.getPosti() + "|" + auto.getDataImmatricolazione() + "\n");
					break;

				case I:

					Imbarcazione imbarcazione = (Imbarcazione) veicolo;
					datiVeicolo.replace(0, datiVeicolo.length(),
							I + "|" + imbarcazione.getTarga().getCodice() + "|" + imbarcazione.getNome() + "|"
									+ imbarcazione.getVelocit�Max().getNumeroMassimaVelocita() + "|"
									+ imbarcazione.getCaricoMax().getNumeroMassimoCarico() + "|"
									+ imbarcazione.getDataImmatricolazione() + "\n");
					break;

				case V:

					Velivolo velivolo = (Velivolo) veicolo;
					datiVeicolo.replace(0, datiVeicolo.length(),
							V + "|" + velivolo.getCodice().getCodice() + "|" + velivolo.getModello().getNome() + "|"
									+ velivolo.getVelocitaKH() + "|" + velivolo.getQuotaMassima() + "|"
									+ velivolo.getDataImmatricolazione() + "\n");
					break;

				}

				bw.write(datiVeicolo.toString());

			}

		}

	};

	public void scriviSuFileVeicoliDatByList(File file, List<Veicolo> veicoli) throws IOException {

		try (Writer writer = new OutputStreamWriter(new FileOutputStream(file));
				BufferedWriter bw = new BufferedWriter(writer);) {

			for (Veicolo veicolo : veicoli) {

				/*
				 * StringBuffer e StringBuilder non lavora tramite reference StringBuffer � piu
				 * lento di StringBuilder ma � ThreadSafe
				 */
				StringBuffer datiVeicolo = new StringBuffer("");

				switch (veicolo.getTipo()) {
				case A:

					Autovettura auto = (Autovettura) veicolo;
					datiVeicolo.replace(0, datiVeicolo.length(),
							A + "|" + auto.getTarga().getCodice() + "|" + auto.getModello().getNome() + "|"
									+ auto.getCilindrata().getDimensioneMotore() + "|" + auto.getKw().getKw() + "|"
									+ auto.getPosti() + "|" + auto.getDataImmatricolazione() + "\n");
					break;

				case I:

					Imbarcazione imbarcazione = (Imbarcazione) veicolo;
					datiVeicolo.replace(0, datiVeicolo.length(),
							I + "|" + imbarcazione.getTarga().getCodice() + "|" + imbarcazione.getNome() + "|"
									+ imbarcazione.getVelocit�Max().getNumeroMassimaVelocita() + "|"
									+ imbarcazione.getCaricoMax().getNumeroMassimoCarico() + "|"
									+ imbarcazione.getDataImmatricolazione() + "\n");
					break;

				case V:

					Velivolo velivolo = (Velivolo) veicolo;
					datiVeicolo.replace(0, datiVeicolo.length(),
							V + "|" + velivolo.getCodice().getCodice() + "|" + velivolo.getModello().getNome() + "|"
									+ velivolo.getVelocitaKH() + "|" + velivolo.getQuotaMassima() + "|"
									+ velivolo.getDataImmatricolazione() + "\n");
					break;

				}

				bw.write(datiVeicolo.toString());

			}

		}

	}
}
