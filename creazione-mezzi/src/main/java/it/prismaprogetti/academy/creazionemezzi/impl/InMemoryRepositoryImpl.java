package it.prismaprogetti.academy.creazionemezzi.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import it.prismaprogetti.academy.creazionemezzi.model.Targa;
import it.prismaprogetti.academy.creazionemezzi.model.Tipo;
import it.prismaprogetti.academy.creazionemezzi.model.Veicolo;
import it.prismaprogetti.academy.creazionemezzi.model.autovettura.Autovettura;
import it.prismaprogetti.academy.creazionemezzi.model.imbarcazione.Imbarcazione;
import it.prismaprogetti.academy.creazionemezzi.model.velivolo.Velivolo;
import it.prismaprogetti.academy.creazionemezzi.repository.InMemoryRepository;
import it.prismaprogetti.academy.creazionemezzi.utility.RandomUtility;

public class InMemoryRepositoryImpl implements InMemoryRepository {

	/**
	 * questo � il mio repository
	 */
	private Map<Targa, Veicolo> veicoliByTarga = new HashMap<>();

	@Override
	public void save(Veicolo veicolo) {

		Tipo tipo = veicolo.getTipo();

		switch (tipo) {
		case A:

			Autovettura autovettura = (Autovettura) veicolo;
			veicoliByTarga.put(autovettura.getTarga(), autovettura);

			break;

		case I:
			
			Imbarcazione imbarcazione = (Imbarcazione) veicolo;
			veicoliByTarga.put(imbarcazione.getTarga(), imbarcazione);

			break;

		case V:

			Velivolo velivolo = (Velivolo) veicolo;
			veicoliByTarga.put(velivolo.getCodice(), velivolo);

		}

	}

	@Override
	public List<Veicolo> getAllVeicoli() {

		Set<Targa> targhe = this.veicoliByTarga.keySet();

		List<Veicolo> veicoli = new ArrayList<>(targhe.size());

		for (Targa targa : targhe) {

			Veicolo veicolo = veicoliByTarga.get(targa);
			veicoli.add(veicolo);
		}

		return veicoli;
	}

	@Override
	public List<Veicolo> getVeicoliCasuali(int numeroVeicoli) {
		
		/*
		 * Set: insieme che non tiene conto dell'ordinamento
		 */
		Set<Targa> targheSet = this.veicoliByTarga.keySet();
		
		if ( targheSet == null ) {
			throw new NullPointerException("il repository � vuoto");
		}
		
		/*
		 * trasformo il set di targhe in list di targhe
		 */
		List<Targa> targheList = new ArrayList<>(targheSet);
		
		List<Veicolo> veicoliCasuali = new ArrayList<>();

		for (int i = 0; i < numeroVeicoli; i++) {

			int randomNumber = RandomUtility.randomNumber(0, targheList.size()-1);

			Targa targa = targheList.get(randomNumber);

			Veicolo veicolo = this.veicoliByTarga.get(targa);

			veicoliCasuali.add(veicolo);
		}

		return veicoliCasuali;
	}

	@Override
	public Veicolo getVeicoloByTarga(String targa) {
		
		Veicolo veicolo = veicoliByTarga.get(targa.trim());
//		if (veicolo == null) {
//			throw new NullPointerException("il veicolo cercato tramite targa: " + targa + " non � presente nel repository");
//		}
		
		return veicolo;
	}

}
