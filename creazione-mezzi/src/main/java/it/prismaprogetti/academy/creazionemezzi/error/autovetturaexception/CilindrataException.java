package it.prismaprogetti.academy.creazionemezzi.error.autovetturaexception;

public class CilindrataException extends Exception {

	private CilindrataException(String message) {
		super(message);
	}
	
	public static CilindrataException cilindrataNonValida() {
		return new CilindrataException("cilindrata non valida");
	}

}
