package it.prismaprogetti.academy.creazionemezzi.model.autovettura;

import it.prismaprogetti.academy.creazionemezzi.model.Modello;

public final class ModelloAutovettura extends Modello {

	private ModelloAutovettura(String nome) {
		super(nome);
	}
	
	public static ModelloAutovettura creaModello(String nome) {
		
		checkIsNotNull(nome);
		
		return new ModelloAutovettura(nome);
		
	}
	
	public static void checkIsNotNull(String nome) {
		if ( nome.isBlank() || nome == null ) {
			throw new NullPointerException("nome nullo");
		}
	}

}
