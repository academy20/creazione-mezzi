package it.prismaprogetti.academy.creazionemezzi.model.imbarcazione;

import static it.prismaprogetti.academy.creazionemezzi.model.Tipo.I;

import it.prismaprogetti.academy.creazionemezzi.model.DataImmatricolazione;
import it.prismaprogetti.academy.creazionemezzi.model.Targa;
import it.prismaprogetti.academy.creazionemezzi.model.Tipo;
import it.prismaprogetti.academy.creazionemezzi.model.Veicolo;
import it.prismaprogetti.academy.creazionemezzi.model.autovettura.Autovettura;
import it.prismaprogetti.academy.creazionemezzi.model.autovettura.Cilindrata;
import it.prismaprogetti.academy.creazionemezzi.model.autovettura.ModelloAutovettura;
import it.prismaprogetti.academy.creazionemezzi.model.autovettura.Potenza;
import it.prismaprogetti.academy.creazionemezzi.model.autovettura.TargaAutovettura;

public class Imbarcazione extends Veicolo {

	// Tipo
	private TargaImbarcazione targa;
	private String nome;
	private VelocitaMax velocitÓMax;
	private CaricoMax caricoMax;
	// Data Imm

	private Imbarcazione(Tipo tipo, DataImmatricolazione dataImmatricolazione, TargaImbarcazione targa, String nome,
			VelocitaMax velocitÓMax, CaricoMax caricoMax) {
		super(tipo, dataImmatricolazione);
		this.targa = targa;
		this.nome = nome;
		this.velocitÓMax = velocitÓMax;
		this.caricoMax = caricoMax;
	}

	public TargaImbarcazione getTarga() {
		return targa;
	}

	public String getNome() {
		return nome;
	}

	public VelocitaMax getVelocitÓMax() {
		return velocitÓMax;
	}

	public CaricoMax getCaricoMax() {
		return caricoMax;
	}

	public static Imbarcazione creaImbarcazione(DataImmatricolazione dataImmatricolazione, TargaImbarcazione targa, String nome, VelocitaMax velocitaMax, CaricoMax caricoMax) {

		return new Imbarcazione(I, dataImmatricolazione, targa, nome, velocitaMax, caricoMax);

	}

}
