package it.prismaprogetti.academy.creazionemezzi.model;

public enum Tipo {
	
	A, V, I;
	
	/*
	 * public static final Tipo A = new Tipo(),
	 * V = new Tipo(),
	 * I = new Tipo();
	 * private Tipo () {}
	 */

}
