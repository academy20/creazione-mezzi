package it.prismaprogetti.academy.creazionemezzi.model.velivolo;

import it.prismaprogetti.academy.creazionemezzi.model.DataImmatricolazione;

import it.prismaprogetti.academy.creazionemezzi.model.Tipo;
import it.prismaprogetti.academy.creazionemezzi.model.Veicolo;

public class Velivolo extends Veicolo {

	private CodiceVelivolo codice;// fatto
	private ModelloVelivolo modello;// dal modello prendo anche l'uso, la velocita e la quota massima
	private UsoVelivolo usoVelivolo;
	private int velocitaKH;
	private int quotaMassima;

	private Velivolo(Tipo tipo, CodiceVelivolo codice, ModelloVelivolo modello, UsoVelivolo usoVeivolo, int velocitaKH,
			int quotaMassima, DataImmatricolazione dataImmatricolazione) {
		super(tipo, dataImmatricolazione);
		this.codice = codice;
		this.modello = modello;
		this.usoVelivolo = usoVeivolo;
		this.velocitaKH = velocitaKH;
		this.quotaMassima = quotaMassima;
	}

	public ModelloVelivolo getModello() {
		return modello;
	}

	public UsoVelivolo getUsoVeivolo() {
		return usoVelivolo;
	}

	public int getVelocitaKH() {
		return velocitaKH;
	}

	public int getQuotaMassima() {
		return quotaMassima;
	}

	public CodiceVelivolo getCodice() {
		return codice;
	}

	public static Velivolo creaVelivolo(Tipo tipo, CodiceVelivolo codice, ModelloVelivolo modello,
			UsoVelivolo usoVelivolo, int velocitaKH, int quotaMassima, DataImmatricolazione dataImmatricolazione) {

		return new Velivolo(tipo, codice, modello, usoVelivolo, velocitaKH, quotaMassima, dataImmatricolazione);

	}

}
