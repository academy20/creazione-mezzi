package it.prismaprogetti.academy.creazionemezzi.error;

public class DataException extends Exception{

	private DataException(String message) {
		super(message);
	}

	public static DataException dataInvalida() {
		return new DataException("data non valida");
	}
}
