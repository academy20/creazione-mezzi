package it.prismaprogetti.academy.creazionemezzi.error;

public class TargaException extends Exception {

	private TargaException(String message) {
		super(message);
	}

	public static TargaException formatoTargaNonValido(String targa) {
		return new TargaException("formato targa non valido: " + targa);
	}

}
