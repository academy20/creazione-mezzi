package it.prismaprogetti.academy.creazionemezzi.model.imbarcazione;

import it.prismaprogetti.academy.creazionemezzi.error.imbarcazioneexception.CaricoMaxException;
import it.prismaprogetti.academy.creazionemezzi.error.imbarcazioneexception.VelocitaMaxException;

public class VelocitaMax {

	private int numeroMassimaVelocita;

	private VelocitaMax(int numeroMassimaVelocita) {
		super();
		this.numeroMassimaVelocita = numeroMassimaVelocita;
	}

	public int getNumeroMassimaVelocita() {
		return numeroMassimaVelocita;
	}

	public static void checkNumeroMassimaVelocita(int velocita) throws VelocitaMaxException {

		if (velocita > 70) {

			throw VelocitaMaxException.velocitaMaxNonValida();
		}

	}

	public static VelocitaMax creazioneVelocitaMassimaImbarcazione(String velocita)
			throws CaricoMaxException, VelocitaMaxException {

		int velocitaMax = Integer.parseInt(velocita.trim());

		checkNumeroMassimaVelocita(velocitaMax);

		return new VelocitaMax(velocitaMax);
	}

}
