package it.prismaprogetti.academy.creazionemezzi.error.veivoloexception;



public class QuotaMassimaException extends Exception {

	
	private QuotaMassimaException(String message) {
		super(message);
	}
	
	public static QuotaMassimaException caricoMaxNonValido() {
		return new QuotaMassimaException("La quota Massima � stata superata");
	}
}
