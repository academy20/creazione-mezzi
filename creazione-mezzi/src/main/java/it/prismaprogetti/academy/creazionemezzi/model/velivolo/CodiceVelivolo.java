package it.prismaprogetti.academy.creazionemezzi.model.velivolo;

import java.util.Random;

import it.prismaprogetti.academy.creazionemezzi.model.Targa;

public class CodiceVelivolo extends Targa{


	private CodiceVelivolo(String codice) {
		super(codice);
	}

	public String getCodice() {
		return codice;
	}

	public static CodiceVelivolo creaCodiceVelivoloRandom() {

		/*
		 * il codice deve avere 2 lettere e 6 numeri
		 */
		String codice = "";
		/*
		 * creo 2 caratteri random e 6 numeri random e compongo la Stringa
		 */
		Random random = new Random();
		char carattereRandom1 = (char) (random.nextInt(26) + 'a');
		char carattereRandom2 = (char) (random.nextInt(26) + 'a');
		codice = Character.toString(carattereRandom1);
		codice = codice + Character.toString(carattereRandom2);
		for (int i = 0; i < 6; i++) {
			int numeroRandom = random.nextInt(1 + 10);
			codice = codice + String.valueOf(numeroRandom);
		}

		return new CodiceVelivolo(codice);

	}

}
