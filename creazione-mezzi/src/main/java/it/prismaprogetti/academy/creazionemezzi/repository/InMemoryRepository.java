package it.prismaprogetti.academy.creazionemezzi.repository;

import java.util.List;
import java.util.Set;

import it.prismaprogetti.academy.creazionemezzi.model.Targa;
import it.prismaprogetti.academy.creazionemezzi.model.Veicolo;

public interface InMemoryRepository {

	public void save(Veicolo veicolo);
	
	public List<Veicolo> getAllVeicoli();
	
	public List<Veicolo> getVeicoliCasuali(int numeroVeicoli);
	
	public Veicolo getVeicoloByTarga(String targa);	
}
