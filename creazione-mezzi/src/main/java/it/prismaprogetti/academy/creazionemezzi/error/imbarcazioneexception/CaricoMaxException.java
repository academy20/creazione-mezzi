package it.prismaprogetti.academy.creazionemezzi.error.imbarcazioneexception;

public class CaricoMaxException extends Exception{

	private CaricoMaxException(String message) {
		super(message);
	}
	
	public static CaricoMaxException caricoMaxNonValido() {
		return new CaricoMaxException("Carico Massimo non valido");
	}
	
}
