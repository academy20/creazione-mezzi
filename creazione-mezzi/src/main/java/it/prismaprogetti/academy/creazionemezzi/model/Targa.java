package it.prismaprogetti.academy.creazionemezzi.model;

public abstract class Targa {

	protected String codice;

	protected Targa(String codice) {
		super();
		this.codice = codice;
	}

	public String getCodice() {
		return codice;
	}

	public static void checkIsNotNull(String targa) {

		if (targa == null || targa.isBlank()) {
			throw new NullPointerException("targa necessaria");
		}

	} 

}
