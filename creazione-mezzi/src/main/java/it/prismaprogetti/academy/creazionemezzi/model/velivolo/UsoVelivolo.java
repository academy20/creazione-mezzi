package it.prismaprogetti.academy.creazionemezzi.model.velivolo;

public enum UsoVelivolo {

	/**
	 * M = Militare, C = Civile;
	 */
	M, C;

}
