package it.prismaprogetti.academy.creazionemezzi.model;

public abstract class Veicolo {

	private Tipo tipo;
	private DataImmatricolazione dataImmatricolazione;

	protected Veicolo(Tipo tipo, DataImmatricolazione dataImmatricolazione) {
		super();
		this.tipo = tipo;
		this.dataImmatricolazione = dataImmatricolazione;
	}

	public Tipo getTipo() {
		return tipo;
	}

	public DataImmatricolazione getDataImmatricolazione() {
		return dataImmatricolazione;
	}

}
