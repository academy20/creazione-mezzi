package it.prismaprogetti.academy.creazionemezzi.model;

public abstract class Modello {

	private String nome;

	protected Modello(String nome) {
		super();
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

}
