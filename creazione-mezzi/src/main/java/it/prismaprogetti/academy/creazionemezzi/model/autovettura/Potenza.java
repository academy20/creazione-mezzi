package it.prismaprogetti.academy.creazionemezzi.model.autovettura;

import it.prismaprogetti.academy.creazionemezzi.error.autovetturaexception.CilindrataException;
import it.prismaprogetti.academy.creazionemezzi.error.autovetturaexception.PotenzaException;

public class Potenza {

	private int kw;

	private Potenza(int kw) {
		super();
		this.kw = kw;
	}

	public int getKw() {
		return kw;
	}

	public static int checkPotenza(String potenza) throws PotenzaException {

		/*
		 * potrebbe sollevare un NumberFormatException
		 */
		Integer kw = Integer.parseInt(potenza.trim());

		if (kw < 0 || kw > 5000) {

			throw PotenzaException.potenzaNonValida();
		}

		return kw;

	}

	public static Potenza creazionePotenzaAutovettura(String motore)
			throws CilindrataException, PotenzaException {

		int kw = checkPotenza(motore);

		return new Potenza(kw);
	}

}
