package it.prismaprogetti.academy.creazionemezzi.error.imbarcazioneexception;

public class VelocitaMaxException extends Exception {


	public VelocitaMaxException(String message) {
		super(message);
	}

	public static VelocitaMaxException velocitaMaxNonValida() {
		return new VelocitaMaxException("velocit� non valida");
	}
	
}
