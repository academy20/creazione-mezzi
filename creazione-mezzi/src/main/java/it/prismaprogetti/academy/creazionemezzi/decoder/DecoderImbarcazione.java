package it.prismaprogetti.academy.creazionemezzi.decoder;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import it.prismaprogetti.academy.creazionemezzi.error.TargaException;
import it.prismaprogetti.academy.creazionemezzi.error.imbarcazioneexception.CaricoMaxException;
import it.prismaprogetti.academy.creazionemezzi.error.imbarcazioneexception.VelocitaMaxException;
import it.prismaprogetti.academy.creazionemezzi.model.imbarcazione.CaricoMax;
import it.prismaprogetti.academy.creazionemezzi.model.imbarcazione.ImbarcazioneConCaratteristiche;
import it.prismaprogetti.academy.creazionemezzi.model.imbarcazione.TargaImbarcazione;
import it.prismaprogetti.academy.creazionemezzi.model.imbarcazione.VelocitaMax;

public class DecoderImbarcazione extends DecoderVeicoli {

	public DecoderImbarcazione(char marcatore) {
		super(marcatore);
	}

	public List<TargaImbarcazione> readTarghe(String path) throws IOException, TargaException {

		List<TargaImbarcazione> targheImbarcazioni = new ArrayList<>();

		try (Reader reader = new FileReader(new File(path)); BufferedReader br = new BufferedReader(reader)) {

			String targa = null;
			while ((targa = br.readLine()) != null) {

				TargaImbarcazione targaImbarcazione = TargaImbarcazione.creaTargaImbarcazione(targa);

				targheImbarcazioni.add(targaImbarcazione);
			}

		}
		return targheImbarcazioni;

	}

	public List<ImbarcazioneConCaratteristiche> readImbarcazioniConCaratteristiche(String path)
			throws IOException, CaricoMaxException, VelocitaMaxException {

		List<ImbarcazioneConCaratteristiche> imbarcazioniConCaratteristiche = new ArrayList<>();

		try (Reader reader = new FileReader(new File(path)); BufferedReader br = new BufferedReader(reader)) {

			/*
			 * recupero i record
			 */
			String record = null;
			while ((record = br.readLine()) != null) { // Ferrari F430, 4300, 350, 2

				String[] recordArray = record.split(String.valueOf(this.getMarcatore())); // [Ferrari F430 | 4300 | 350
																							// | 2]
				if (recordArray.length != 3) {
					System.err.println("il record � invalido " + record);
					continue;
				}

				String nome = recordArray[0].trim();
				VelocitaMax velocitaMax = VelocitaMax.creazioneVelocitaMassimaImbarcazione(recordArray[1].trim());
				CaricoMax caricoMax = CaricoMax.creazioneCaricoMassimoImbarcazione(recordArray[2].trim());

				ImbarcazioneConCaratteristiche imbarcazioneConCaratteristiche = new ImbarcazioneConCaratteristiche(nome,
						velocitaMax, caricoMax);
				imbarcazioniConCaratteristiche.add(imbarcazioneConCaratteristiche);
			}

		}
		return imbarcazioniConCaratteristiche;

	}

}
