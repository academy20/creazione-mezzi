package it.prismaprogetti.academy.creazionemezzi.run;

import java.io.File;
import java.io.IOException;
import java.util.List;

import it.prismaprogetti.academy.creazionemezzi.decoder.DecoderVeicoli;
import it.prismaprogetti.academy.creazionemezzi.decoder.DecoderAutomobili;
import it.prismaprogetti.academy.creazionemezzi.decoder.DecoderImbarcazione;
import it.prismaprogetti.academy.creazionemezzi.decoder.DecoderVelivoli;
import it.prismaprogetti.academy.creazionemezzi.error.DataException;
import it.prismaprogetti.academy.creazionemezzi.error.TargaException;
import it.prismaprogetti.academy.creazionemezzi.error.autovetturaexception.CilindrataException;
import it.prismaprogetti.academy.creazionemezzi.error.autovetturaexception.PotenzaException;
import it.prismaprogetti.academy.creazionemezzi.error.imbarcazioneexception.CaricoMaxException;
import it.prismaprogetti.academy.creazionemezzi.error.imbarcazioneexception.VelocitaMaxException;
import it.prismaprogetti.academy.creazionemezzi.impl.InMemoryRepositoryImpl;
import it.prismaprogetti.academy.creazionemezzi.model.VeicoliManager;
import it.prismaprogetti.academy.creazionemezzi.model.autovettura.Autovettura;
import it.prismaprogetti.academy.creazionemezzi.model.imbarcazione.Imbarcazione;
import it.prismaprogetti.academy.creazionemezzi.model.velivolo.Velivolo;
import it.prismaprogetti.academy.creazionemezzi.repository.InMemoryRepository;

public class Main {

	public static void main(String[] args) {

		/*
		 * paths in comune
		 */
		String pathDateImmatricolazioni =  "src\\main\\resources\\dateimmatricolazioni";
		File file = new File(pathDateImmatricolazioni);
		System.out.println(file.exists());
		/*
		 * paths auto
		 */
		String pathModelliAutoConCaratteristiche = "src\\main\\resources\\modelliautoconcaratteristiche";
		String targheAuto = "src\\main\\resources\\targheauto";
		
		/*
		 * paths velivoli (aerei)
		 */
		String pathModelliVelivoli = "src\\main\\resources\\modellivelivoli";
		
		/*
		 * paths imbarcazioni
		 */
		String pathImbarcazioniConCaratteristiche ="src\\main\\resources\\imbarcazioniconcaratteristiche";
		String targheImbarcazioni = "src\\main\\resources\\targheimbarcazioni";
		
		/*
		 * path scrivi su file
		 */
		String directoryDoveScrivereIlFileVeicoli = "src\\main\\resources\\";
		
		/*
		 * creazione Repository
		 */
		InMemoryRepository inMemoryRepository = new InMemoryRepositoryImpl();
		
		/*
		 * creazione decoderAutomobili e decoderVelivoli
		 */
		DecoderVeicoli decoderVeicoli = new DecoderVeicoli(','); // decoder dei veicoli
		DecoderAutomobili decoderAutomobili = new DecoderAutomobili(',');
		DecoderVelivoli decoderVelivoli = new DecoderVelivoli(',');
		DecoderImbarcazione decoderImbarcazione = new DecoderImbarcazione(',');
		
		/*
		 * istanzio un gestore per i veicoli
		 */
		VeicoliManager veicoliManager = null;
		try {
			veicoliManager = VeicoliManager.crea(decoderVeicoli, directoryDoveScrivereIlFileVeicoli, pathDateImmatricolazioni);
		} catch (IOException | DataException e1) {
			e1.printStackTrace();
		}
		
		/*
		 * creo le autovetture e velivoli tramite i suddetti paths
		 */
		try {
			List<Autovettura> autovetture = veicoliManager.creaAutomobiliDaFile(decoderAutomobili, pathModelliAutoConCaratteristiche, targheAuto);
			List<Imbarcazione> imbarcazioni = veicoliManager.creaImbarcazioniDaFile(decoderImbarcazione, pathImbarcazioniConCaratteristiche, targheImbarcazioni);
			List<Velivolo> velivoli = veicoliManager.creaVelivoliDaFile(decoderVelivoli, pathModelliVelivoli);
			
			/*
			 * inserisci le autovetture nel db
			 */
			for (Autovettura autovettura : autovetture) {
				
				VeicoliManager.inserisciVeicoloNelRepository(inMemoryRepository, autovettura);
			}
			
			/*
			 * inserisco i velivoli nel db
			 */
			for ( Velivolo velivolo : velivoli) {
				
				VeicoliManager.inserisciVeicoloNelRepository(inMemoryRepository, velivolo);
			}
			
			/*
			 * inserisco le imbarcazioni nel db
			 */
			for( Imbarcazione imbarcazione : imbarcazioni) {
				
				
				VeicoliManager.inserisciVeicoloNelRepository(inMemoryRepository, imbarcazione);
			}
			
		} catch (IOException | CilindrataException | PotenzaException | TargaException | DataException | CaricoMaxException | VelocitaMaxException e) {

			System.err.println("errore::"+e.getMessage());
			System.err.println("///////////////////////////////////////////////////");
			e.printStackTrace();
		}
			
		/*
		 * creo il file veicoli in formato Dat e scrivo i veicoli all'interno del file
		 */
		try {
			veicoliManager.creaFileVeicoliDatEScriviVeicoli(inMemoryRepository.getVeicoliCasuali(20));
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
