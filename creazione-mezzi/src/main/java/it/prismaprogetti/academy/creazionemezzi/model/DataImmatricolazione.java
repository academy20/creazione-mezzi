package it.prismaprogetti.academy.creazionemezzi.model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.regex.Pattern;

import it.prismaprogetti.academy.creazionemezzi.error.DataException;

/**
 * Pattern data yyyyMMgg
 * @author patri
 *
 */
public class DataImmatricolazione {

	private LocalDate data;

	public LocalDate getData() {
		return data;
	}

	private DataImmatricolazione(LocalDate data) {
		super();
		this.data = data;
	}

	public static DataImmatricolazione creaData(String data) throws DataException {

		/*
		 * check
		 */
		checkFormatData(data);

		/*
		 * creazione data
		 */
		
		LocalDate localDate = convertStringToData(data);
		
		return new DataImmatricolazione(localDate);
	}
	
	@Override
	public String toString() {
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd").withLocale(Locale.ITALY);
		
		return this.data.format(formatter);
	}

	public static void checkIsNotNull(String data) {
		if (data.trim().isBlank() || data.trim() == null) {
			throw new NullPointerException("data non pu� essere nullo");
		}
	}

	public static void checkFormatData(String data) throws DataException {

		/*
		 * verifico se la stringa passata � composta esattamente da 8 numeri totali
		 */
		if (!Pattern.matches("\\d{8}", data)) {
			throw DataException.dataInvalida();
		}
	}

	/**
	 * metodo che verifica se i numeri formano una data valida
	 */
	public static LocalDate convertStringToData(String data) throws DataException {

		checkFormatData(data);

		Integer mese = Integer.parseInt(data.substring(4, 6)); // 20120124 --> parseInt(01) --> 1
		if (mese < 1 || mese > 12) {
			throw DataException.dataInvalida();
		}

		Integer giorno = Integer.parseInt(data.substring(6, 8));
		if (giorno < 1 || giorno > 31) {
			throw DataException.dataInvalida();
		}

		Integer anno = Integer.parseInt(data.substring(0, 4));
		boolean bisestile = (anno > 1584 && ((anno % 400 == 0) || (anno % 4 == 0 && anno % 100 != 0)));

		boolean meseCon30Giorni = (mese == 4 || mese == 6 || mese == 9 || mese == 11);
		boolean meseCon31Giorni = (mese == 1 || mese == 3 || mese == 5 || mese == 7 || mese == 8 || mese == 10
				|| mese == 12);

		if (meseCon30Giorni) {

			if (giorno > 30) {
				throw DataException.dataInvalida();
			}

		}

		if (meseCon31Giorni) {

			if (giorno > 31) {
				throw DataException.dataInvalida();
			}

		}

		if (mese == 2) {

			if (bisestile) {

				if (giorno > 29) {
					throw DataException.dataInvalida();
				}

			} else {

				if (giorno > 28) {
					throw DataException.dataInvalida();
				}

			}

		}
		return LocalDate.of(anno, mese, giorno);

	}
	
	

}
