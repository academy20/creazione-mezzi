package it.prismaprogetti.academy.creazionemezzi.model.autovettura;

import static it.prismaprogetti.academy.creazionemezzi.model.Tipo.A;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import it.prismaprogetti.academy.creazionemezzi.decoder.DecoderAutomobili;
import it.prismaprogetti.academy.creazionemezzi.error.DataException;
import it.prismaprogetti.academy.creazionemezzi.error.TargaException;
import it.prismaprogetti.academy.creazionemezzi.error.autovetturaexception.CilindrataException;
import it.prismaprogetti.academy.creazionemezzi.error.autovetturaexception.PotenzaException;
import it.prismaprogetti.academy.creazionemezzi.model.DataImmatricolazione;
import it.prismaprogetti.academy.creazionemezzi.model.Targa;
import it.prismaprogetti.academy.creazionemezzi.model.Tipo;
import it.prismaprogetti.academy.creazionemezzi.model.Veicolo;;

public class Autovettura extends Veicolo {

	private TargaAutovettura targa;
	private ModelloAutovettura modello;
	private Cilindrata cilindrata;
	private Potenza kw;
	private int posti;

	private Autovettura(Tipo tipo, DataImmatricolazione dataImmatricolazione, TargaAutovettura targa, ModelloAutovettura modello,
			Cilindrata cilindrata, Potenza kw, int posti) {
		super(tipo, dataImmatricolazione);
		this.targa = targa;
		this.modello = modello;
		this.cilindrata = cilindrata;
		this.kw = kw;
		this.posti = posti;
	}

	public TargaAutovettura getTarga() {
		return targa;
	}

	public ModelloAutovettura getModello() {
		return modello;
	}

	public Cilindrata getCilindrata() {
		return cilindrata;
	}

	public Potenza getKw() {
		return kw;
	}

	public int getPosti() {
		return posti;
	}

	public static Autovettura creaAutomobile(DataImmatricolazione dataImmatricolazione, TargaAutovettura targa,
			ModelloAutovettura modello, Cilindrata cilindrata, Potenza kw, int posti) {

		return new Autovettura(A, dataImmatricolazione, targa, modello, cilindrata, kw, posti);

	}

	@Override
	public String toString() {
		return "Autovettura [targa=" + targa.getCodice() + ", modello=" + modello.getNome() + ", cilindrata=" + cilindrata.getDimensioneMotore() + ", kw=" + kw.getKw()
				+ ", posti=" + posti + "]";
	}
	
	
	

}
