package it.prismaprogetti.academy.creazionemezzi.decoder;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import it.prismaprogetti.academy.creazionemezzi.error.DataException;
import it.prismaprogetti.academy.creazionemezzi.model.DataImmatricolazione;
import it.prismaprogetti.academy.creazionemezzi.model.Tipo;
import it.prismaprogetti.academy.creazionemezzi.model.velivolo.CodiceVelivolo;
import it.prismaprogetti.academy.creazionemezzi.model.velivolo.ModelloVelivolo;
import it.prismaprogetti.academy.creazionemezzi.model.velivolo.UsoVelivolo;
import it.prismaprogetti.academy.creazionemezzi.model.velivolo.Velivolo;
import it.prismaprogetti.academy.creazionemezzi.utility.RandomUtility;

public class DecoderVelivoli extends DecoderVeicoli {

	public DecoderVelivoli(char marcatore) {
		super(marcatore);
	}

	public List<Velivolo> readVelivoliFromFile(String pathvelivoli) throws IOException, DataException {

		List<Velivolo> velivoli = new ArrayList<>();

		try (Reader reader = new FileReader(new File(pathvelivoli));

				BufferedReader br = new BufferedReader(reader)) {

			/*
			 * recupero i record
			 */
			String record = null;
			while ((record = br.readLine()) != null) {

				String[] recordArray = record.split(String.valueOf(this.getMarcatore()));

				/*
				 * prendo il modello
				 */
				String nomeModello = recordArray[0];

				/*
				 * prendo l'uso e verifico se � C=Civile M=Militare
				 */
				UsoVelivolo usoVelivolo;
				String usoVelicoloString = recordArray[1];

				if (usoVelicoloString.equals(UsoVelivolo.C.name())) {
					usoVelivolo = UsoVelivolo.C;
				} else {
					usoVelivolo = UsoVelivolo.M;
				}

				/*
				 * prendo la velocit�
				 */
				int velocitaKH = Integer.parseInt(recordArray[2]);

				/*
				 * prendo la quota massima
				 */
				int quotaMassima = Integer.parseInt(recordArray[3]);

				/*
				 * creo un modelloVelivolo
				 */
				ModelloVelivolo modelloVelivolo = ModelloVelivolo.creaModello(nomeModello);

				/*
				 * genero casualmente il codice(Targa) e la data di immatricolazione
				 */
				CodiceVelivolo codVelivolo = CodiceVelivolo.creaCodiceVelivoloRandom();
				DataImmatricolazione dataImmatricolazione = RandomUtility.randomData();

				/*
				 * creo il velivolo
				 */
				Velivolo velivolo = Velivolo.creaVelivolo(Tipo.V, codVelivolo, modelloVelivolo, usoVelivolo, velocitaKH,
						quotaMassima, dataImmatricolazione);

				/*
				 * lo aggiungo alla lista
				 */
				velivoli.add(velivolo);

			}

		}

		return velivoli;

	}

}
