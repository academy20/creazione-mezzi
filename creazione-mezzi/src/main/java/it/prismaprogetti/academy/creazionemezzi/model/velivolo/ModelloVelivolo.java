package it.prismaprogetti.academy.creazionemezzi.model.velivolo;

import it.prismaprogetti.academy.creazionemezzi.model.Modello;

public final class ModelloVelivolo extends Modello {

	private ModelloVelivolo(String nome) {
		super(nome);

	}

	public static ModelloVelivolo creaModello(String nome) {

		checkIsNotNull(nome);

		return new ModelloVelivolo(nome);

	}

	public static void checkIsNotNull(String nome) {
		if (nome.isBlank() || nome == null) {
			throw new NullPointerException("nome null");
		}

	}

}
