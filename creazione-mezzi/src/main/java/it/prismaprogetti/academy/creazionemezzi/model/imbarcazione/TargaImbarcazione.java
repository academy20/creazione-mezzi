package it.prismaprogetti.academy.creazionemezzi.model.imbarcazione;

import java.util.regex.Pattern;

import it.prismaprogetti.academy.creazionemezzi.error.TargaException;
import it.prismaprogetti.academy.creazionemezzi.model.Targa;

public class TargaImbarcazione extends Targa {

	private TargaImbarcazione(String codice) {
		super(codice);
	}

	public static void checkFormatoTargaImbarcazione(String targa) throws TargaException {

		if (!Pattern.matches("[A-Z]{2}\\d{5}", targa.trim())) {
			throw TargaException.formatoTargaNonValido(targa);
		}

	}

	public static TargaImbarcazione creaTargaImbarcazione(String targa) throws TargaException {

		checkIsNotNull(targa);
		checkFormatoTargaImbarcazione(targa);
		
		return new TargaImbarcazione(targa);

	}

}
