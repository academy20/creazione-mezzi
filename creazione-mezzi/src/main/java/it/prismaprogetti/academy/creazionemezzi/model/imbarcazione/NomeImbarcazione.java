package it.prismaprogetti.academy.creazionemezzi.model.imbarcazione;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.ClosedFileSystemException;
import java.util.ArrayList;
import java.util.Random;

public class NomeImbarcazione {

	
	private String nome;
	private ArrayList<NomeImbarcazione> nomiImbarcazioni=new ArrayList<NomeImbarcazione>();

	public NomeImbarcazione() {
	}
	
	private NomeImbarcazione(String nome) {
		this.nome=nome;
	}

	/**
	 * questo metodo non va bene
	 * 
	 * @param fileNomiImbarcazioni
	 * @return
	 * @throws IOException
	 */
	// private List<NomeImbarcazione> recuperaNomiImbarcazioniDaFileTramiteDecoder(Decoder decoder)
	private ArrayList<NomeImbarcazione> leggiFileNomiImbarcazioni(File fileNomiImbarcazioni) throws IOException {
		
			ArrayList<String>listaNomiImbarcazione=new ArrayList<String>();
			BufferedReader leggiDocumento = new BufferedReader(new FileReader(fileNomiImbarcazioni));
			String rigaLetta=null;
			
			try {
				
				while((rigaLetta = leggiDocumento.readLine())!= null) {
					listaNomiImbarcazione.add(rigaLetta);
				}
			}catch (IOException e) {
				throw new IOException("File nomi non trovato");
			}finally {
				try {
					leggiDocumento.close();
				}catch(ClosedFileSystemException e) {
					throw new IOException("Problema con la chiusura del file");
				}
		}
			for(int i=0; i<listaNomiImbarcazione.size();i++) {
				String nome=listaNomiImbarcazione.get(i);
				this.nomiImbarcazioni.add(new NomeImbarcazione(nome.trim()));
			}
				
			return nomiImbarcazioni;
	}
	
	
	
	private NomeImbarcazione nomeRandom(File path) throws IOException{
			
			leggiFileNomiImbarcazioni(path);
			
			Random rand = new Random();
			int indiceRandom = rand.nextInt((nomiImbarcazioni.size() - 1) + 1);
			return nomiImbarcazioni.get(indiceRandom);
	}
	
	public NomeImbarcazione creaNome(File path) throws IOException {
		return nomeRandom(path);
	}

	public String getTarga() {
		return nome;
	}
}