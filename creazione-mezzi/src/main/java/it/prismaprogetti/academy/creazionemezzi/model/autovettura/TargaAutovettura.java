package it.prismaprogetti.academy.creazionemezzi.model.autovettura;

import java.util.regex.Pattern;

import it.prismaprogetti.academy.creazionemezzi.error.TargaException;
import it.prismaprogetti.academy.creazionemezzi.model.Targa;

public final class TargaAutovettura extends Targa {

	private TargaAutovettura(String codice) {
		super(codice);
	}

	public static TargaAutovettura creaTarga(String targa) throws TargaException {
		
		checkIsNotNull(targa);
		checkFormatoTarga(targa);

		return new TargaAutovettura(targa);

	}

	public static void checkFormatoTarga(String targa) throws TargaException {

		checkIsNotNull(targa);

		/*
		 * prendo la targa tolgo gli spazi bianchi se la lunghezza � diversa da 7 allora
		 * sollevo eccezione: utilizzo un metodo statico con un msg gi� preimpostato
		 * 
		 * inoltre se la targa passata non rispetta il seguente formato XX123XX solleva
		 * eccezione
		 */
		if (!Pattern.matches("[A-Z]{2}\\d{3}[A-Z]{2}", targa.trim())) {
			throw TargaException.formatoTargaNonValido(targa);
		}

	}

}
