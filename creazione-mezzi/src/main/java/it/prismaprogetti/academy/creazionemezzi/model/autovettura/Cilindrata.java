package it.prismaprogetti.academy.creazionemezzi.model.autovettura;

import it.prismaprogetti.academy.creazionemezzi.error.autovetturaexception.CilindrataException;

public class Cilindrata {

	private int dimensioneMotore;

	private Cilindrata(int dimensioneMotore) {
		super();
		this.dimensioneMotore = dimensioneMotore;
	}

	public int getDimensioneMotore() {
		return dimensioneMotore;
	}

	public static int checkDimensioneMotore(String dimensioneMotore) throws CilindrataException {

		/*
		 * potrebbe sollevare un NumberFormatException
		 */
		Integer cilindrata = Integer.parseInt(dimensioneMotore.trim());

		if (cilindrata < 500 || cilindrata > 5000) {

			throw CilindrataException.cilindrataNonValida();
		}

		return cilindrata;

	}

	public static Cilindrata creazioneMotoreAutovettura(String motore) throws CilindrataException {

		int cilindrata = checkDimensioneMotore(motore);

		return new Cilindrata(cilindrata);
	}

}
