package it.prismaprogetti.academy.creazionemezzi.model.imbarcazione;

/**
 * 
 * DTO DATE TRANSFER OBJECT serve per raggruppare piu info insieme
 */
public class ImbarcazioneConCaratteristiche {

	private String nome;
	private VelocitaMax velocitaMax;
	private CaricoMax caricoMax;

	public ImbarcazioneConCaratteristiche(String nome, VelocitaMax velocitaMax, CaricoMax caricoMax) {
		super();
		this.nome = nome;
		this.velocitaMax = velocitaMax;
		this.caricoMax = caricoMax;
	}

	public String getNome() {
		return nome;
	}

	public VelocitaMax getVelocitaMax() {
		return velocitaMax;
	}

	public CaricoMax getCaricoMax() {
		return caricoMax;
	}

}
