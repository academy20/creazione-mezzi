package it.prismaprogetti.academy.creazionemezzi.error.autovetturaexception;

public class PotenzaException extends Exception {

	
	
	public PotenzaException(String message) {
		super(message);
	}

	public static PotenzaException potenzaNonValida() {
		return new PotenzaException("potenza non valida");
	}

}
