package it.prismaprogetti.academy.creazionemezzi.utility;

import java.util.GregorianCalendar;
import java.util.Random;

import it.prismaprogetti.academy.creazionemezzi.error.DataException;
import it.prismaprogetti.academy.creazionemezzi.model.DataImmatricolazione;

public class RandomUtility {

	public static int randomNumber(int min, int max) {

		return (int) Math.floor(Math.random() * ((max - min + 1) + min));
	}

	public static DataImmatricolazione randomData() throws DataException {

		/*
		 * genera una data random
		 */
		String data = "";

		GregorianCalendar gc = new GregorianCalendar();
		int annoAttuale = gc.get(GregorianCalendar.YEAR);
		/*
		 * mi prendo la differenza tra l'anno attuale e il 1980(anno minimo di
		 * immatricolazione)
		 */
		int differenzaAnno = annoAttuale - 1980;
		/*
		 * genera un anno random tra il 1980 e il 2022
		 */
		int randomAnno = (int) (Math.random() * differenzaAnno + 1980);

		int mese = (int) (Math.random() * 12 + 1);
		int giorno = gc.get(GregorianCalendar.DATE);
		int nuovoGiorno = 0;
		Random aRandom = new Random();

		// numero casuale da 1 a 31
		long range = 31;
		long fraction = (long) (range * aRandom.nextDouble());
		nuovoGiorno = (int) (fraction + 1);

		// se il mese � da 30 giorni e il numero generato � 31
		if (mese == 4 || mese == 6 || mese == 9 || mese == 11 && nuovoGiorno > 30) {
			mese = mese + 1; // passo al mese seguente
			nuovoGiorno = 1; // imposto il giorno come primo del mese
		}
		// se il mese � febbraio e il numero casuale � > di 28
		else if (mese == 2 && nuovoGiorno > 28) {
			mese = mese + 1; // passo al mese seguente
			nuovoGiorno = 1; // imposto il giorno come primo del mese
		}
		// se nuovoGiorno � precedente al giorno attuale
		if (nuovoGiorno <= giorno)
			mese++; // porto avanti il mese di scadenza di 1
		// se il nuovo mese risulta essere 13 ovvero un numero non valido
		if (mese == 13) {
			mese = 1; // passo a Gennaio
			randomAnno++; // passo all'anno seguente
		}

		if (mese < 10 && nuovoGiorno < 10)
			data = randomAnno + "0" + mese + "0" + nuovoGiorno;
		else if (mese < 10 && giorno >= 10)
			data = randomAnno + "0" + mese + "" + nuovoGiorno;
		else if (mese >= 10 && nuovoGiorno < 10)
			data = randomAnno + "" + mese + "0" + nuovoGiorno;
		else
			data = randomAnno + "" + mese + "" + giorno;

		return DataImmatricolazione.creaData(data);
	}

}
