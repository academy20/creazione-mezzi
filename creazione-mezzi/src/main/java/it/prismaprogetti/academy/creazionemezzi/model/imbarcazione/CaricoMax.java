package it.prismaprogetti.academy.creazionemezzi.model.imbarcazione;

import it.prismaprogetti.academy.creazionemezzi.error.imbarcazioneexception.CaricoMaxException;

public class CaricoMax {

	private int numeroMassimoCarico;

	private CaricoMax(int numeroMassimoCarico) {
		super();
		this.numeroMassimoCarico = numeroMassimoCarico;
	}

	public int getNumeroMassimoCarico() {
		return numeroMassimoCarico;
	}

	public static int checkNumeroMassimoCarico(int carico) throws CaricoMaxException {

		if (carico < 1 || carico > 7000) {

			throw CaricoMaxException.caricoMaxNonValido();
		}

		return carico;

	}

	public static CaricoMax creazioneCaricoMassimoImbarcazione(String carico) throws CaricoMaxException {

		int caricoMax = Integer.parseInt(carico);

		checkNumeroMassimoCarico(caricoMax);

		return new CaricoMax(caricoMax);
	}

}
